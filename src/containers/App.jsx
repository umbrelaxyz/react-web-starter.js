import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { testReduxAction } from 'actions'
import { App } from 'components'

// const mapStateToProps = (state) => ({

// })

const mapDispatchToProps = {
  testReduxAction
}

AppContainer.propTypes = {
  testReduxAction: PropTypes.func.isRequired
}
function AppContainer ({ testReduxAction }) {
  return (
    <App onTestReduxClicked={testReduxAction} />
  )
}

export default connect(null, mapDispatchToProps)(AppContainer)
