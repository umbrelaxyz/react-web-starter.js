import React, { Component, PropTypes } from 'react'
import styled from 'styled-components'
import { colors } from 'theme'

const Container = styled.div `
  text-align: center;
`

const Header = styled.div `
  background-color: ${colors.primary};
  height: 150px;
  padding: 20px;
  color: white;
`

const Intro = styled.p `
  font-size: large;
`

const Button = styled.button `
  background-color: #D1C4E9;
  color: ${colors.primary}
  width: 200px;
  height: 50px;
  border: none;
  border-radius: 20px;
  font-size: 1.5rem;
  cursor: pointer;
`

class App extends Component {
  static propTypes = {
    onTestReduxClicked: PropTypes.func.isRequired
  }

  render () {
    const { onTestReduxClicked } = this.props
    return (
      <Container>
        <Header>
          <h2>Welcome to React</h2>
          <Button onClick={onTestReduxClicked}>
            Test Redux
          </Button>
        </Header>
        <Intro>
          Project skeleton made from "ejected" create-react-app w/ HRM, redux, and reactotron.
        </Intro>
      </Container>
    )
  }
}

export default App
