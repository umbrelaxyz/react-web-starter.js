/**
 *  Helper to let you write reducer as an object
 */
export default function createReducer (initialState = {}, actionMap = {}) {
  return (state = initialState, action) => {
    const mapper = actionMap[action.type]
    return mapper ? mapper(state, action) : state
  }
}
