import { combineReducers } from 'redux'
import { createReducer } from './helpers'
import { types } from 'actions'

const initialState = 0
const clickCount = createReducer(initialState, {

  [types.TEST_CLICK_ACTION]: (state) => {
    console.log('types.TEST_CLICK_ACTION: ', state)
    return state + 1
  },

})

export default combineReducers({
  clickCount
})
