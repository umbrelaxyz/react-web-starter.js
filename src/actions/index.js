export const types = {
  TEST_CLICK_ACTION: '@index.js/TEST_CLICK_ACTION'
}

export function testReduxAction () {
  return async (dispatch) => {
    dispatch({ type: types.TEST_CLICK_ACTION })
  }
}
