import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { AppContainer } from 'react-hot-loader'
// Need to require this directly for hot reloading to work
import App from './containers/App'
import createStore from 'config/createStore'
import 'config/ReactotronConfig'
import 'theme/reset.css'

const initialState = {}
const reduxStore = createStore(initialState)

const rootEl = document.getElementById('root')

if (process.env.NODE_ENV !== 'development') {
  ReactDOM.render(
    <Provider store={reduxStore}>
      <App />
    </Provider>,
    rootEl
  )
}
else {
  // Need AppContainer wrapper for hot reloading
  // https://github.com/gaearon/react-hot-boilerplate/blob/next/src/index.js

  ReactDOM.render(
    <AppContainer>
      <Provider store={reduxStore}>
        <App />
      </Provider>
    </AppContainer>,
    rootEl
  )
}

if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const NextApp = require('./containers/App').default
    ReactDOM.render(
      <AppContainer>
        <Provider store={reduxStore}>
          <NextApp />
        </Provider>
      </AppContainer>,
      rootEl
    )
  })
}
