import { createStore, applyMiddleware, compose } from 'redux'
import Reactotron from 'reactotron-react-js'
import thunk from 'redux-thunk'
import rootReducer from 'reducers'

export default function (initialState = {}) {
  const enhancer = compose(
    applyMiddleware(thunk),
  )

  const store = process.env.NODE_ENV === 'development'
    ? Reactotron.createStore(rootReducer, initialState, enhancer)
    : createStore(rootReducer, initialState, enhancer)

  // use HMR for reducers
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('../reducers/index').default
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
