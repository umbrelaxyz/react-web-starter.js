import Reactotron, { trackGlobalErrors } from 'reactotron-react-js'
import { reactotronRedux } from 'reactotron-redux'

Reactotron
  .configure({ name: 'React Skeleton App' })
  .use(reactotronRedux())
  .use(trackGlobalErrors({
    veto: (frame) => frame.fileName.indexOf('/node_modules/react-native/') >= 0
  }))
  .connect()
